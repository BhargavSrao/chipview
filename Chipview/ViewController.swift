

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet var txtTag: UITextField!
    @IBOutlet var tbllist: UITableView!
    var aryTeglist = [String]()
    var aryCopyTags = [String]()
    
    @IBOutlet var tagListView: TaglistCollection!
    
    @IBOutlet weak var tagheight: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tagListView.layer.backgroundColor = UIColor.black.cgColor
        
        tagListView.layer.borderWidth = 1
        tagListView.layer.borderColor = UIColor.black.cgColor
        self.title = "chipview"
        txtTag.delegate = self
        
        txtTag.addTarget(self, action: #selector(ViewController.textFieldDidChange(_:)), for: .editingDidEndOnExit)
        
        
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.setupTaglistView()
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    
    //Target Functions
    @objc func textFieldDidChange(_ textField: UITextField) {
        let strText = self.txtTag.text?.trimmingCharacters(in: .whitespaces)
        
        
        if(strText?.count != 0){
            tagheight.constant =   tagListView.frame.size.height + 30
            tagheight?.isActive = true
            self.tagListView.appendTag(tagName: strText!)
            self.aryTeglist.append(strText!)
            self.txtTag.text = ""
        }
    }
    
    
    func setupTaglistView() {
        
        self.tagListView.setupTagCollection(height : tagListView.frame.size.height )
        self.tagListView.delegate = self
        
        self.aryTeglist = self.tagListView.copyAllTags()
        
        self.tagListView.textFont = UIFont.systemFont(ofSize: 15.0, weight: .heavy)
        
    }
    
    @IBAction func copySelectedAction(_ sender: UIButton) {
        self.aryCopyTags.removeAll()
        self.aryCopyTags = self.tagListView.copySelectedTags()
        self.tbllist.reloadData()
        
    }
    @IBAction func copyUnselectedAction(_ sender: UIButton) {
        self.aryCopyTags.removeAll()
        self.aryCopyTags = self.tagListView.copyUnselectedTags()
        self.tbllist.reloadData()
    }
    @IBAction func addTagAction(_ sender: UIButton) {
        let strText = self.txtTag.text?.trimmingCharacters(in: .whitespaces)
        if(strText?.count != 0){
            tagheight.constant =   tagListView.frame.size.height + 30
            tagheight?.isActive = true
            self.tagListView.appendTag(tagName: strText!)
            self.aryTeglist.append(strText!)
            self.txtTag.text = ""
        }
        
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        // do something when backspace is tapped/entered in an empty text field
        
        if string == " " {
            let strText = self.txtTag.text?.trimmingCharacters(in: .whitespaces)
            if(strText?.count != 0){
                tagheight.constant =   tagListView.frame.size.height + 30
                tagheight?.isActive = true
                self.tagListView.appendTag(tagName: strText!)
                self.aryTeglist.append(strText!)
                self.txtTag.text = ""
            }
        }
        else
        {
            print("arratname",string)
            
        }
        return true;
    }
    
    
}

extension ViewController : TagViewDelegate {
    func didRemoveTag(_ indexPath: IndexPath) {
        print("RemoveIndexPath==\(indexPath)")
        tagheight.constant =   tagListView.frame.size.height  - 30
        tagheight?.isActive = true
        
    }
    
    func didTaponTag(_ indexPath: IndexPath) {
        print("sdsad")
        print("Tag tapped: \(self.aryTeglist[indexPath.item])")
    }
    
}




